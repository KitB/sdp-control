#!/usr/bin/env python
import pygtk
pygtk.require('2.0')
import gtk
from Controllers import Communications, Vision, Strategy
from Strategy.Params import Params

class Control:


    # get the parameters from the radio buttons and entry, then compile the vision
    def make_vision(self, widget):
        print "Making vision"
        port = int(self.vision_entry_port.get_text())
        if self.vision_radio_pitch_main.get_active():
            pitch = 0
        else:
            pitch = 1
        if self.vision_radio_mplayer_true.get_active():
            use_mplayer = True
        else:
            use_mplayer = False
        video_device_number = 1
        self.vision_process = Vision(port, pitch, use_mplayer, video_device_number)
        print "Vision compiled"
        # Enable clicking of the "Start Vision" button. This is how you change the sensitivity of a gtk.ToggleButton.
        self.vision_button_make.props.sensitive = True

    # Actually start the vision.
    def vision(self, widget):
        if widget.get_active():
            print "Vision starting"
            self.vision_process.start()
            self.vision_button_make.set_label("Stop vision")
            # While vision is running, enable progression to the comms. Disable the "Compile Vision" button.
            self.comms_button_make.set_sensitive(True)
            self.vision_button_make.set_sensitive(False)
        else:
            print "Vision stopping"
            self.vision_process.stop()
            self.vision_button_make.set_label("Start vision")
            # If vision has been stopped, allow the user to recompile vision.
            self.vision_button_make.set_sensitive(True)


    # Compile comms
    def make_comms(self, widget):
        print "Making comms"
        self.comms_process = Communications()
        self.comms_process.recompile()
        print "Comms compiled"
        self.comms_button.props.sensitive = True

    # Launch the comms, unlock Strategy.
    def comms(self, widget):
        if widget.get_active():
            print "Comms starting"
            self.comms_process.start()
            self.comms_button.set_label("Stop comms")
            self.strategy_button_get.set_sensitive(True)
            self.comms_button_make.set_sensitive(False)
        else:
            print "Comms stopping"
            self.comms_process.stop()
            self.comms_button_make.set_sensitive(True)
            self.comms_button.set_label("Start comms")


    # Initialise strategy with the entry box data, populate combo box with a list of strategies.
    def get_strategies(self, widget):
        self.strategy_process = Strategy()
        host = self.strategy_entry_host.get_text()
        port = int(self.strategy_entry_port.get_text())
        self.strategy_process.makeWorld(host,port)
        self.strategy_list = self.strategy_process.getStrategies()
        for i in self.strategy_list:
            self.strategy_combo.append_text(i)
            self.strategy_button_make.set_sensitive(True)
        print "Strategy compiled"

    # Check that a strategy is selected, set the variables.
    def make_strategy(self, widget):
        model = self.strategy_combo.get_model()
        active = self.strategy_combo.get_active()
        if active < 0:
            print "No strategy selected!"
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_WARNING, gtk.BUTTONS_OK, "You have not selected a strategy!")
            messagedialog.run()
            messagedialog.destroy()   
            return      
        else:
            self.strategy_strat = model[active][0]
            if self.strategy_radio_side_low.get_active():
                self.strategy_process.setSide("low")
            else:
                self.strategy_process.setSide("high")
            if self.strategy_radio_player_blue.get_active():
                self.strategy_process.setPlayer("b")
            else:
                self.strategy_process.setPlayer("y")
            self.strategy_process.setStrategy(self.strategy_strat)
        print "Making strategy"
        self.strategy_button.props.sensitive = True    

    # Start the strategy.
    def strategy(self, widget):
        if widget.get_active():
            print "Strategy starting"
            self.strategy_process.start()
            self.strategy_button.set_label("Stop strategy")
        else:
            print "Strategy stopping"
            self.strategy_process.stop()
            self.strategy_button.set_label("Start strategy")


    # Update the labels that reflect what is typed in the entry box
    def updates(self, widget):
        text = widget.get_text()
        if widget == self.strategy_entry_host:
            self.strategy_label_connection_host.set_label("Host: " + text)
        elif widget == self.strategy_entry_port:
            self.strategy_label_connection_port.set_label("Port: " + text)
        elif widget == self.vision_entry_port:
            self.vision_label_port.set_label("Port: " + text)


    # Quit function that ensures all processes are ended before we actually quit.
    def quit(self, event=None, widget=None):
        print "Closing"
        if self.vision_button.get_active() == True:
            self.vision_process.stop()
        if self.comms_button.get_active() == True:
            self.comms_process.stop()
        if self.strategy_button.get_active() == True:
            self.strategy_process.stop()
        Params.stop()
        gtk.main_quit()

    # When help is clicked, spawn a dialog box with 3 buttons. When one of those buttons is clicked, create a message box with information.
    def help(self, widget):
        label = gtk.Label("What do you need help with?")
        dialog = gtk.Dialog("Select a category",
                            None,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            )
        dialog.vbox.pack_start(label)
        dialog.add_buttons("Vision", 1, "Comms", 2, "Strategy", 3)
        response = dialog.run()
        dialog.destroy()

        if (response == 1):
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "How to run the Vision.")
            messagedialog.format_secondary_text("First click 'compile vision'. This readies the vision system. Click start to proceed. "
                                                "This lets you begin comms.")
            messagedialog.run()
            messagedialog.destroy()

        elif (response == 2):
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "How to run the Comms.")
            messagedialog.format_secondary_text("Once vision is running, comms is started next. As before, compile then start. This"
                                                " lets you start the strategy in the next tab.")
            messagedialog.run()
            messagedialog.destroy()
        else:
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "How to run the Strategy.")
            messagedialog.format_secondary_text("The strategy needs the vision and communications servers running first. \n"
                                                "Once these are functional, click 'get stratgies' to populate the combo box with a list "
                                                "of available strategies. Select one, click 'compile strategy' and you are ready to begin. Click"
                                                " 'Start Video' to start")
            messagedialog.run()
            messagedialog.destroy()


    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_title("Control GUI")
        self.window.connect("delete_event", self.quit)
        self.window.set_border_width(10)

        self.box_main = gtk.VBox(False, 10)

        self.notebook = gtk.Notebook()

        self.box_vision = gtk.VBox(False, 10)
        self.box_vision_buttons = gtk.HBox(False, 0)
        self.box_vision_port = gtk.HBox(False, 5)
        self.box_vision_pitch = gtk.HBox(False, 5)
        self.box_vision_mplayer = gtk.HBox(False, 5)

        self.box_comms = gtk.VBox(False, 10)
        self.box_comms_buttons = gtk.HBox(False, 0)

        self.box_strategy = gtk.VBox(False, 10)
        self.box_strategy_connection_text = gtk.HBox(False, 0)
        self.box_strategy_connection = gtk.HBox(False, 0)
        self.box_player_radios = gtk.HBox(False, 5)
        self.box_side_radios = gtk.HBox(False, 5)
        self.box_strategy_buttons = gtk.HBox(False, 0)

        self.box_tools = gtk.HBox(False, 10)


        self.vision_label = gtk.Label("Vision")
        self.comms_label = gtk.Label("Comms")
        self.strategy_label = gtk.Label("Strategy")

        self.vision_frame = gtk.Frame()       
        self.vision_frame.add(self.box_vision)
        self.notebook.append_page(self.vision_frame, self.vision_label)

        self.comms_frame = gtk.Frame()
        self.comms_frame.add(self.box_comms)
        self.notebook.append_page(self.comms_frame, self.comms_label)

        self.strategy_frame = gtk.Frame()
        self.strategy_frame.add(self.box_strategy)
        self.notebook.append_page(self.strategy_frame, self.strategy_label)
      

        self.vision_button_make = gtk.Button("Compile vision")
        self.vision_button_make.connect("clicked", self.make_vision)

        self.vision_separator = gtk.HSeparator() 

        self.vision_button = gtk.ToggleButton("Start vision")
        self.vision_button.connect("toggled", self.vision)
        self.vision_button.props.sensitive = False

        self.vision_label_port = gtk.Label("Port: 6789")

        self.vision_entry_port = gtk.Entry()
        self.vision_entry_port.set_text("6789")
        self.vision_entry_port.connect("activate", self.updates)

        self.vision_label_pitch = gtk.Label("Which pitch are you using?")
        self.vision_radio_pitch_main = gtk.RadioButton(group=None, label="Main")
        self.vision_radio_pitch_side = gtk.RadioButton(group=self.vision_radio_pitch_main, label="Side")

        self.vision_label_mplayer = gtk.Label("Are you using mplayer?")
        self.vision_radio_mplayer_true = gtk.RadioButton(group=None, label="True")
        self.vision_radio_mplayer_false = gtk.RadioButton(group=self.vision_radio_mplayer_true, label="False")


        self.box_vision_buttons.pack_start(self.vision_button_make, True, True, 0)
        self.box_vision_buttons.pack_start(self.vision_separator, True, True, 0)
        self.box_vision_buttons.pack_start(self.vision_button, True, True, 0)

        self.box_vision_port.pack_start(self.vision_label_port, True, True, 0)
        self.box_vision_port.pack_start(self.vision_entry_port, True, True, 0)

        self.box_vision_pitch.pack_start(self.vision_label_pitch, True, True, 0)
        self.box_vision_pitch.pack_start(self.vision_radio_pitch_main, True, True, 0)
        self.box_vision_pitch.pack_start(self.vision_radio_pitch_side, True, True, 0)

        self.box_vision_mplayer.pack_start(self.vision_label_mplayer, True, True, 0)
        self.box_vision_mplayer.pack_start(self.vision_radio_mplayer_true, True, True, 0)
        self.box_vision_mplayer.pack_start(self.vision_radio_mplayer_false, True, True, 0)

        self.box_vision.pack_start(self.box_vision_buttons, True, True, 0)
        self.box_vision.pack_start(self.box_vision_port, True, True, 0)
        self.box_vision.pack_start(self.box_vision_pitch, True, True, 0)
        self.box_vision.pack_start(self.box_vision_mplayer, True, True, 0)



        self.comms_button = gtk.ToggleButton("Start comms")
        self.comms_button.connect("toggled", self.comms)
        self.comms_button.props.sensitive = False

        self.comms_button_make = gtk.Button("Compile comms")
        self.comms_button_make.connect("clicked", self.make_comms)
        self.comms_button_make.set_sensitive(False)

        self.comms_separator = gtk.HSeparator() 

        self.box_comms_buttons.pack_start(self.comms_button_make, False, True, 0)
        self.box_comms_buttons.pack_start(self.comms_separator, True, True, 0)
        self.box_comms_buttons.pack_start(self.comms_button, False, True, 0)

        self.box_comms.pack_start(self.box_comms_buttons, False, True, 0)


        self.strategy_label_connection_host = gtk.Label("Host: localhost")
        self.strategy_label_connection_port = gtk.Label("Port: 6789")

        self.strategy_entry_host = gtk.Entry()
        self.strategy_entry_host.set_text("localhost")
        self.strategy_entry_host.connect("activate", self.updates)

        self.strategy_entry_port = gtk.Entry()
        self.strategy_entry_port.set_text("6789")
        self.strategy_entry_port.connect("activate", self.updates)

        self.strategy_player_label = gtk.Label("What player are you?")
        self.strategy_radio_player_blue = gtk.RadioButton(group=None, label="Blue")
        self.strategy_radio_player_yellow = gtk.RadioButton(group=self.strategy_radio_player_blue, label="Yellow")

        self.strategy_radio_side_low = gtk.RadioButton(group=None, label="Low")
        self.strategy_radio_side_high = gtk.RadioButton(group=self.strategy_radio_side_low, label="High")
        self.strategy_side_label = gtk.Label("What side are you?")

        self.strategy_button_get = gtk.Button("Get Strategies")
        self.strategy_button_get.connect("clicked", self.get_strategies)
        self.strategy_button_get.set_sensitive(False)

        self.strategy_button_make = gtk.Button("Prepare strategy")
        self.strategy_button_make.connect("clicked", self.make_strategy)
        self.strategy_button_make.set_sensitive(False)

        self.strategy_button = gtk.ToggleButton("Start strategy")
        self.strategy_button.connect("toggled", self.strategy)
        self.strategy_button.props.sensitive = False

        self.strategy_combo = gtk.combo_box_new_text()


        self.box_strategy_connection_text.pack_start(self.strategy_label_connection_host, True, True, 0)
        self.box_strategy_connection_text.pack_start(self.strategy_label_connection_port, True, True, 0)

        self.box_strategy_connection.pack_start(self.strategy_entry_host, True, True, 0)
        self.box_strategy_connection.pack_start(self.strategy_entry_port, True, True, 0)

        self.box_player_radios.pack_start(self.strategy_player_label, True, True, 0)
        self.box_player_radios.pack_start(self.strategy_radio_player_blue, True, True, 0)
        self.box_player_radios.pack_start(self.strategy_radio_player_yellow, True, True, 0)

        self.box_side_radios.pack_start(self.strategy_side_label, True, True, 0)
        self.box_side_radios.pack_start(self.strategy_radio_side_low, True, True, 0)
        self.box_side_radios.pack_start(self.strategy_radio_side_high, True, True, 0)

        self.box_strategy_buttons.pack_start(self.strategy_button_get, True, True, 0)
        self.box_strategy_buttons.pack_start(self.strategy_button_make, True, True, 0)
        self.box_strategy_buttons.pack_start(self.strategy_button, True, True, 0)

        self.box_strategy.pack_start(self.box_strategy_connection_text, True, True, 0)
        self.box_strategy.pack_start(self.box_strategy_connection, True, True, 0)
        self.box_strategy.pack_start(self.box_player_radios, True, True, 0)
        self.box_strategy.pack_start(self.box_side_radios, True, True, 0)
        self.box_strategy.pack_start(self.box_strategy_buttons, True, True, 0)
        self.box_strategy.pack_start(self.strategy_combo, True, True, 0)

        self.button_quit = gtk.Button("Quit")
        self.button_quit.connect("clicked", self.quit)

        self.button_help = gtk.Button("Help")
        self.button_help.connect("clicked", self.help)

        self.box_tools.pack_start(self.button_quit, True, True, 0)
        self.box_tools.pack_start(self.button_help, True, True, 0)
        self.box_main.pack_start(self.notebook, True, True, 0)
        self.box_main.pack_start(self.box_tools, True, True, 0)

        self.window.add(self.box_main)
        self.window.show_all()

    def run(self):
        gtk.main()
