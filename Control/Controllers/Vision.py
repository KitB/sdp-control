""" Hacked together vision starter """

import subprocess
import os

class Vision:
    port = None
    pitch = None
    use_mplayer = None
    video_device_number = None
    _subp = None
    def __init__(self, port=None, pitch=None, use_mplayer=None, video_device_number=None):
        self.port = port
        self.pitch = pitch
        self.use_mplayer = use_mplayer
        self.video_device_number = video_device_number
        os.chdir(os.path.dirname(__file__))
        os.chdir("../Vision")

    def start(self):
        cmd = "./start_vision_server.sh"
        if self.port != None:
            cmd += " --port=%d" % self.port
        if self.pitch != None:
            cmd += " --pitch=%d" % self.pitch
        if self.use_mplayer:
            cmd += " --use-mplayer"
        if self.video_device_number != None:
            cmd += " --video-device-number=%d" % self.video_device_number
        print cmd
        self.cmd = cmd
        self._subp = subprocess.Popen(cmd, shell=True)

    def stop(self):
        self._subp.terminate()
        subprocess.Popen("pgrep -f serve_vision.py | xargs kill", shell=True)
