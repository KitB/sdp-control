from multiprocessing import Process
import time

from ..Strategy.Utils.Default import string2Strategy
from ..Strategy.Model.World import World
from ..Strategy.Communication.Communication import RobotControl
from ..Strategy.Communication.VisionClient import VisionClient

class Strategy:
    strategy = None
    world = None
    robot = None
    _subp = None
    def __init__(self):
        pass

    def getStrategies(self):
        return string2Strategy.getKeys()

    def setStrategy(self, strategy):
        assert strategy in self.getStrategies()
        self.strategy_function = string2Strategy[strategy]

    def makeStrategy(self):
        self.strategy = self.strategy_function(self.world, self.robot)

    def makeWorld(self, host, port):
        visionSource = VisionClient(host=host, tryPort=port)
        world = World(visionSource, True)
        world.setPlayer(self.world.getPlayer())
        world.setSide(self.world.getSide())
        self.world = world
        self.makeStrategy()

    def setSide(self, side):
        self.world.setSide(side)

    def setPlayer(self, player):
        self.world.setPlayer(player)

    def getWorld(self):
        return self.world

    def makeRobotControl(self, host, port):
        self.robot = RobotControl(host, port)
        self.makeStrategy()

    def getRobotControl(self):
        return self.robot

    def start(self):
        """ Starts the strategy running in a subprocess """
        self._subp = Process(target=self.strategy)
        self._subp.start()

    def stop(self):
        """ Stops the strategy subprocess """
        self.strategy.done.value = True
        time.sleep(1)
        self._subp.terminate()
