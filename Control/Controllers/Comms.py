import subprocess
import os

class Communications:
    _subp = None
    def __init__(self):
        os.chdir(os.path.dirname(__file__))
        os.chdir("../Comms")

    def start(self):
        cmd = "./run_comms.sh"
        self._subp = subprocess.Popen(cmd, shell=True)

    def stop(self):
        self._subp.terminate()
        subprocess.Popen("pgrep -f CommunicationServer | xargs kill -2", shell=True)

    def recompile(self):
        cmd = "./make_comms.sh"
        subprocess.Popen(cmd, shell=True)
